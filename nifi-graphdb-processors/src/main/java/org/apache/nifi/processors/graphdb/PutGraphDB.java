/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nifi.processors.graphdb;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
//import org.apache.nifi.annotation.behavior.ReadsAttribute;
//import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.expression.AttributeExpression;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.SequenceInputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Random;
import javax.security.auth.login.LoginException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@EventDriven
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"put", "graphdb", "rdf", "ttl", "turtle"})
@CapabilityDescription("Reads triples from an incoming FlowFile, and writes them to the specified GraphDB repository." +
        " If any error occurs while reading records from the input, or writing records to GraphDB, the FlowFile will be routed to failure")
//@SeeAlso({})
//@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="triple.count", description="Number of triples written to GraphDB")})

public class PutGraphDB extends AbstractProcessor {

    public static final PropertyDescriptor GRAPHDB_URL = new PropertyDescriptor
            .Builder().name("graphdb-url")
            .displayName("GraphDB URL")
            .description("URL of the GraphDB REST API endpoint")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_REPOSITORY = new PropertyDescriptor
            .Builder().name("graphdb-repo")
            .displayName("GraphDB Repository")
            .description("Name of the GraphDB repository to import the data to")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_TARGET_GRAPH = new PropertyDescriptor
            .Builder().name("graphdb-named-graph")
            .displayName("Target Graph")
            .description("Name of the named graph to import the data to")
            .required(false)
            .defaultValue("default")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_REPLACE_EXISTING_DATA = new PropertyDescriptor
            .Builder().name("graphdb-replace-existing-data")
            .displayName("Replace existing data")
            .description("Enable replacement of existing data")
            .defaultValue("false")
            .required(true)
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_KEEP_UPLOADED_FILE = new PropertyDescriptor
            .Builder().name("graphdb-keep-uploaded-file")
            .displayName("Keep uploaded file")
            .description("Keep uploaded file in GraphDB")
            .defaultValue("false")
            .required(true)
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_FILE_IMPORT_TIMEOUT_SECS = new PropertyDescriptor
            .Builder().name("graphdb-file-import-timeout-secs")
            .displayName("File import timeout (s)")
            .description("File import timeout in seconds")
            .defaultValue("10")
            .required(true)
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_USERNAME = new PropertyDescriptor
            .Builder().name("graphdb-username")
            .displayName("GraphDB Username")
            .description("The username to use when logging into GraphDB")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING))
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GRAPHDB_PASSWORD = new PropertyDescriptor
            .Builder().name("graphdb-password")
            .displayName("GraphDB Password")
            .description("The password to use when logging into GraphDB")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .sensitive(true)
            .build();


    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("A FlowFile is routed to this relationship after it has been successfully stored in GraphDB")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("A FlowFile is routed to this relationship if it failed to be stored in GraphDB")
            .build();


    public static final String TRIPLE_COUNT_ATTR = "triple.count";

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(GRAPHDB_URL);
        descriptors.add(GRAPHDB_REPOSITORY);
        descriptors.add(GRAPHDB_TARGET_GRAPH);
        descriptors.add(GRAPHDB_REPLACE_EXISTING_DATA);
        descriptors.add(GRAPHDB_KEEP_UPLOADED_FILE);
        descriptors.add(GRAPHDB_FILE_IMPORT_TIMEOUT_SECS);
        descriptors.add(GRAPHDB_USERNAME);
        descriptors.add(GRAPHDB_PASSWORD);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    // Properties set in onScheduled.
    private volatile String url;
    private volatile String repo;
    private volatile String targetGraph;
    private volatile boolean replaceExistingData;
    private volatile boolean keepUploadedFile;
    private volatile int fileImportTimeoutSecs;
    private volatile String authorization;
    private volatile HttpClient client;

    @OnScheduled
    public void onScheduled(final ProcessContext context) throws LoginException {
        url = context.getProperty(GRAPHDB_URL).evaluateAttributeExpressions().getValue();
        repo = context.getProperty(GRAPHDB_REPOSITORY).evaluateAttributeExpressions().getValue();
        targetGraph = context.getProperty(GRAPHDB_TARGET_GRAPH).evaluateAttributeExpressions().getValue();
        replaceExistingData = context.getProperty(GRAPHDB_REPLACE_EXISTING_DATA).evaluateAttributeExpressions().asBoolean();
        keepUploadedFile = context.getProperty(GRAPHDB_KEEP_UPLOADED_FILE).evaluateAttributeExpressions().asBoolean();
        fileImportTimeoutSecs = context.getProperty(GRAPHDB_FILE_IMPORT_TIMEOUT_SECS).evaluateAttributeExpressions().asInteger();

        client = HttpClient.newHttpClient();

        authenticate(context.getProperty(GRAPHDB_USERNAME)
                            .evaluateAttributeExpressions().getValue(),
                     context.getProperty(GRAPHDB_PASSWORD).getValue());
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }

        Map<String, Integer> repoSize;
        try {
            repoSize = getRepoSize();
        } catch (IOException | InterruptedException | RuntimeException e) {
            getLogger().error("Failed to get repo size due to {}", new Object[]{e.getMessage()});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }
        int triplesBefore = repoSize.get("explicit");

        try {
            uploadFile(session, flowFile, fileImportTimeoutSecs);
        } catch (IOException | InterruptedException | RuntimeException e) {
            getLogger().error("Failed to upload file due to {}", new Object[]{e.getMessage()});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }

        try {
            repoSize = getRepoSize();
        } catch (IOException | InterruptedException | RuntimeException e) {
            getLogger().error("Failed to get repo size due to {}", new Object[]{e.getMessage()});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }
        int triplesAfter = repoSize.get("explicit");
        session.putAttribute(flowFile, TRIPLE_COUNT_ATTR,
                             String.valueOf(triplesAfter - triplesBefore));

        if (!keepUploadedFile) {
            try {
                deleteUploadedFile(flowFile);
            } catch (IOException | InterruptedException | RuntimeException e) {
                getLogger().error("Failed to delete uploaded file due to {}", new Object[]{e.getMessage()});
                session.transfer(flowFile, REL_FAILURE);
                return;
            }
        }

        session.transfer(flowFile, REL_SUCCESS);
        session.getProvenanceReporter().send(flowFile, "Successfully added FlowFile to GraphDB");
    }

    // internal methods
    private void authenticate(String username, String password)
                 throws LoginException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/rest/login/" + username))
                .header("X-GraphDB-Password", password)
                .POST(BodyPublishers.noBody())
                .build();
        HttpResponse<String> response;
        try {
            response = client.send(request, BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new LoginException("Unable to login with " + username + " due to: " + e.getMessage());
        }
        if (response.statusCode() != 200) {
            throw new LoginException("Unable to login with " + username + " - login response has status code " + response.statusCode());
        }
        authorization = response.headers().firstValue("Authorization").orElse(null);
        if (authorization == null) {
            throw new LoginException("Unable to login with " + username + " - login response has no authorization header");
        }
        return;
    }

    private Map<String, Integer> getRepoSize()
                                 throws IOException, InterruptedException, RuntimeException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/rest/repositories/" + repo + "/size"))
                .header("Authorization", authorization)
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        int statusCode = response.statusCode();
        if (statusCode != 200) {
            throw new RuntimeException(String.format("GraphDB /rest/repositories/%s/size returned code %d with message %s",
                                       repo, statusCode, response.body()));
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response.body(), Map.class);
    }

    private void uploadFile(final ProcessSession session, FlowFile flowFile,
                            int fileImportTimeoutSecs)
                 throws IOException, InterruptedException, RuntimeException  {

        String filename = flowFile.getAttribute(CoreAttributes.FILENAME.key());
        Map<String, String> importSettings = Map.of("name", filename,
                                                    "context", targetGraph);

        // build multipart body
        String boundary = "---------------------------" + new BigInteger(100, new Random()).toString();
        List<InputStream> inputStreams = new ArrayList<>();

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PrintWriter out = new PrintWriter(outStream, true, StandardCharsets.UTF_8) {
            @Override
            public void println() { write("\r\n"); }
        };

        // import file content header
        outStream.reset();
        out.println("--" + boundary);
        out.println("Content-Disposition: form-data; name=\"file\"; filename=\"" + filename);
        out.println("Content-Type: application/octet-stream");
        out.println();
        out.flush();
        inputStreams.add(new ByteArrayInputStream(outStream.toByteArray()));

        // input stream from FlowFile
        inputStreams.add(session.read(flowFile));

        // importSettings JSON header
        outStream.reset();
        out.println();
        out.println("--" + boundary);
        out.println("Content-Disposition: form-data; name=\"importSettings\"; filename=\"blob\"");
        out.println("Content-Type: application/json");
        out.println();
        out.flush();
        inputStreams.add(new ByteArrayInputStream(outStream.toByteArray()));

        // importSettings JSON content
        outStream.reset();
        out.println();
        ObjectMapper mapper = new ObjectMapper();
        out.println(mapper.writeValueAsString(importSettings));
        out.flush();
        inputStreams.add(new ByteArrayInputStream(outStream.toByteArray()));

        // closing
        outStream.reset();
        out.println("--" + boundary + "--");
        out.flush();
        inputStreams.add(new ByteArrayInputStream(outStream.toByteArray()));

        out.close();

        SequenceInputStream inputStream = new SequenceInputStream(Collections.enumeration(inputStreams));

        // upload to GraphDB using Workbench REST API
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/rest/data/import/upload/" + repo + "/file"))
                .header("Authorization", authorization)
                .header("Content-Type", "multipart/form-data; boundary=" + boundary)
                .POST(BodyPublishers.ofInputStream(() -> inputStream))
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        String expectedResponse = "\"File " + filename + " sent for import.\"";
        int statusCode = response.statusCode();
        String responseBody = response.body();
        if (statusCode != 202 || !expectedResponse.equals(responseBody)) {
            throw new RuntimeException(String.format("GraphDB /rest/data/import/upload/%s/file returned code %d with message %s",
                                       repo, statusCode, responseBody));
        }

        // check GraphDB processing status
        long timeoutTime = System.currentTimeMillis() + fileImportTimeoutSecs * 1000L;
        request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/rest/data/import/upload/" + repo))
                .header("Authorization", authorization)
                .build();
        String status = null;
        String message = null;
        while (true) {
            response = client.send(request, BodyHandlers.ofString());
            statusCode = response.statusCode();
            if (statusCode != 200) {
                throw new RuntimeException(String.format("GraphDB /rest/data/import/upload/%s returned code %d with message %s",
                                           repo, statusCode, response.body()));
            }
            List<Map<String, Object>> uploads = mapper.readValue(response.body(),
                    new TypeReference<List<Map<String, Object>>>(){});
            for (Map<String, Object> upload : uploads) {
                String name = (String) upload.get("name");
                if (filename.equals(name)) {
                    status = (String) upload.get("status");
                    message = (String) upload.get("message");
                }
            }
            if (!"IMPORTING".equals(status)) {
                break;
            }
            if (System.currentTimeMillis() > timeoutTime) {
                break;
            }
            // wait a couple of seconds before checking again
            Thread.sleep(2000);
        }

        if (!"DONE".equals(status)) {
            if (status == null) {
                throw new RuntimeException(String.format("GraphDB does not have any import entry for file %s", filename));
            } else if ("IMPORTING".equals(status)) {
                throw new RuntimeException(String.format("GraphDB is still importing file %s", filename));
            } else if ("ERROR".equals(status)) {
                throw new RuntimeException(String.format("GraphDB import of file %s failed due to: %s", filename, message));
            } else {
                getLogger().error(String.format("GraphDB /rest/data/import/upload/%s for file %s returned status %s - message %s",
                                  repo, filename, status, message));
            }
        }

        return;
    }

    private void deleteUploadedFile(FlowFile flowFile)
                 throws IOException, InterruptedException, RuntimeException {

        String filename = flowFile.getAttribute(CoreAttributes.FILENAME.key());
        ObjectMapper mapper = new ObjectMapper();
        String deleteFiles = mapper.writeValueAsString(List.of(filename));

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/rest/data/import/upload/" + repo + "/status?remove=true"))
                .header("Authorization", authorization)
                .header("Content-Type", "application/json")
                .method("DELETE", BodyPublishers.ofString(deleteFiles))
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        String expectedResponse = "\"Cleared status.\"";
        int statusCode = response.statusCode();
        String responseBody = response.body();
        if (statusCode != 200 || !expectedResponse.equals(responseBody)) {
            throw new RuntimeException(String.format("GraphDB /rest/data/import/upload/%s/status?remove=true for file %s returned code %d with message %s",
                                       repo, filename, statusCode, responseBody));
        }

        return;
    }
}
